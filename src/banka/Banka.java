/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banka;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author jakub
 */
public class Banka {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        boolean end = false;
        while (!end) {
            System.out.println("Zadej číslo účtu: (pokud nemáš, zadej -1");
            String ucet = sc.next();
            int volba;
            if (ucet.matches("-1")) {
                volba = 6;
            } else {
                printMenu();
                volba = sc.nextInt();
            }
            switch (volba) {
                case 1:
                    System.out.println("Tvůj zůstatek je: " + zustatek(ucet));
                    break;
                case 2:
                    System.out.println("Kolik chceš vložit na účet " + ucet + "?");
                    int vloz = sc.nextInt();
                    vklad(ucet, vloz, dateFormat.format(date));
                    break;
                case 3:
                    System.out.println("Kolik chceš bybrat z účetu " + ucet + "?");
                    int vyber = sc.nextInt();
                    vyber(ucet, vyber, dateFormat.format(date));
                    break;
                case 4:
                    System.out.println(vypisTransakci(ucet));
                    break;
                case 5:
                    sc.nextLine();
                    System.out.println("Zadej počáteční datum");
                    String start = sc.nextLine();
                    System.out.println("Zadej konečné datum");
                    String konec = sc.nextLine();
                    int[] bilance1 = vypisBilance(ucet, start, konec);
                    System.out.format("Bilance za určité období: \n"
                            + "Vklady: %d \n"
                            + "Vybery: %d \n", bilance1[0], bilance1[1]);
                    break;
                case 6:
                    int novy = ThreadLocalRandom.current().nextInt(10, 99999999 + 1);
                    createFile(Integer.toString(novy), dateFormat.format(date));
                    System.out.println("Založen nový účet číslo: " + novy);
                    break;
                case 7:
                    end = true;
            }

        }
        

    }

    private static void printMenu() {
        System.out.println("*********************Menu*************************");
        System.out.println("*1. dotaz na zůstatek                            *");
        System.out.println("*2. vklad hotovosti                              *");
        System.out.println("*3. výběr hotovosti                              *");
        System.out.println("*4. log účtu                                     *");
        System.out.println("*5. Bilance za zadané období                     *");
        System.out.println("*6. Založit nový účet                            *");
        System.out.println("*7. konec                                        *");
        System.out.println("**************************************************");
    }

    private static void createFile(String number, String accountCreateTime) throws FileNotFoundException, IOException {

        File resultFile = new File(number + ".dat");
        if (!resultFile.exists()) { //Přepsat !
            try (DataOutputStream inResult = new DataOutputStream(new FileOutputStream(resultFile))) {
                // inResult.writeUTF(number); 
                inResult.writeInt(0);
            }
            resultFile = new File(number + "_transakce.dat");

            try (DataOutputStream inResult = new DataOutputStream(new FileOutputStream(resultFile))) {
                inResult.writeUTF(accountCreateTime);
                inResult.writeUTF("Založení účtu");
                inResult.writeInt(0);

            }
        }
    }

    private static void vyber(String number, int transakce, String time) throws FileNotFoundException, IOException {
        File resultFile = new File(number);
        int zustatek = 0;
        boolean end = false;

        try (DataInputStream ucet = new DataInputStream(new FileInputStream(number + ".dat"))) {
            while (!end) {
                try {
                    // number = ucet.readUTF();
                    zustatek = ucet.readInt();
                } catch (EOFException e) {
                    end = true;
                }
            }
        }
        zustatek = zustatek - transakce;

        try (DataOutputStream inResult = new DataOutputStream(new FileOutputStream(resultFile + ".dat"))) {
            //inResult.writeUTF(number);//číslo
            inResult.writeInt(zustatek);
        }
        try (DataOutputStream inResult = new DataOutputStream(new FileOutputStream(resultFile + "_transakce.dat", true))) {
            inResult.writeUTF(time);
            inResult.writeUTF("Výběr");
            transakce = 0 - transakce;
            inResult.writeInt(transakce);
        }

    }

    private static void vklad(String number, int transakce, String time) throws FileNotFoundException, IOException {
        File resultFile = new File(number);
        int zustatek = 0;
        boolean end = false;

        try (DataInputStream ucet = new DataInputStream(new FileInputStream(number + ".dat"))) {
            while (!end) {
                try {
                    // number = ucet.readUTF();//číslo
                    zustatek = ucet.readInt();
                } catch (EOFException e) {
                    end = true;
                }
            }
        }
        zustatek = zustatek + transakce;

        try (DataOutputStream inResult = new DataOutputStream(new FileOutputStream(resultFile + ".dat"))) {
            //inResult.writeUTF(number);//číslo
            inResult.writeInt(zustatek);
        }
        try (DataOutputStream inResult = new DataOutputStream(new FileOutputStream(resultFile + "_transakce.dat", true))) {
            inResult.writeUTF(time);
            inResult.writeUTF("Vklad");
            inResult.writeInt(transakce);

        }

    }

    private static int zustatek(String number) throws FileNotFoundException, IOException {

        boolean end = false;

        int zustatek = 0;
        try (DataInputStream ucet = new DataInputStream(new FileInputStream(number + ".dat"))) {
            while (!end) {
                try {
                    //ucet.readUTF();
                    zustatek = ucet.readInt();
                } catch (EOFException e) {
                    end = true;
                }
                return zustatek;
            }
        }
        return -1;
    }

    private static String vypisTransakci(String number) throws FileNotFoundException, IOException {
        boolean end = false;
        StringBuilder str = new StringBuilder();
        try (DataInputStream ucet = new DataInputStream(new FileInputStream(number + "_transakce.dat"))) {

            while (!end) {
                try {
                    str.append(ucet.readUTF()).append(" ").append(ucet.readUTF()).append(" ").append(ucet.readInt()).append("\n");

                } catch (EOFException e) {
                    end = true;
                }
            }
        }

        return str.toString();
    }

    private static int[] vypisBilance(String number) throws FileNotFoundException, IOException {
        boolean end = false;
        int vklady = 0;
        int vybery = 0;
        try (DataInputStream ucet = new DataInputStream(new FileInputStream(number + "_transakce.dat"))) {

            while (!end) {
                try {
                    ucet.readUTF();
                    String tran = ucet.readUTF();
                    if (tran.equals("Vklad")) {
                        vklady = vklady + ucet.readInt();
                    } else if (tran.equals("Výběr")) {
                        vybery = vybery + ucet.readInt();
                    } else {
                        ucet.readInt();
                    }

                } catch (EOFException e) {
                    end = true;
                }
            }
        }
        int[] bilance = new int[2];
        bilance[0] = vklady;
        bilance[1] = vybery;
        return bilance;
    }

    private static int[] vypisBilance(String number, String dateFrom, String dateTo) throws FileNotFoundException, IOException {
        boolean end = false;
        int vklady = 0;
        int vybery = 0;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate from = LocalDate.parse(dateFrom, formatter);
        LocalDate to = LocalDate.parse(dateTo, formatter);
        try (DataInputStream ucet = new DataInputStream(new FileInputStream(number + "_transakce.dat"))) {

            while (!end) {
                try {
                    LocalDate date = LocalDate.parse(ucet.readUTF(), formatter);
                    String tran = ucet.readUTF();
                    if (from.isBefore(date) && to.isAfter(date)) {
                        if (tran.equals("Vklad")) {
                            vklady = vklady + ucet.readInt();
                        } else if (tran.equals("Výběr")) {
                            vybery = vybery + ucet.readInt();
                        } else {
                            ucet.readInt();
                        }
                    } else {
                        ucet.readInt();
                    }

                } catch (EOFException e) {
                    end = true;
                }
            }
        }
        int[] bilance = new int[2];
        bilance[0] = vklady;
        bilance[1] = vybery;
        return bilance;
    }

}
